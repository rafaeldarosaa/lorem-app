const express = require("express");
const Paragraph = require("./controllers/Paragraph");

const routes = express.Router();

routes.post("/content", Paragraph.generate);

module.exports = routes;
